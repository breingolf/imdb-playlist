import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

// import { MatSliderModule } from '@angular/material/slider';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';

import { AppCoreModule } from '../core/app-core.module';
import { WatchListComponent } from './watch-list/watch-list.component';

@NgModule({
  declarations: [
    WatchListComponent,
  ],
  imports: [
//    MatSliderModule,
    MatGridListModule,
    MatInputModule,
    CommonModule,
    AppCoreModule,
    FormsModule,
  ],
  exports: [
    WatchListComponent,
  ],
})
export class AppPagesModule { }
