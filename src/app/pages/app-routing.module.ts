import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WatchListComponent } from './watch-list/watch-list.component';

const routes: Routes = [
  {
    path: '',
    component: WatchListComponent
  },
];

@NgModule({
  imports: [ RouterModule.forRoot( routes,) ], // { enableTracing: true } ) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
