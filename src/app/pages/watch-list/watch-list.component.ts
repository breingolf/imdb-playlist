import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef } from '@angular/core';

import { MatGridList } from '@angular/material/grid-list';

import { Movie } from '@core/objects/movie';
import { MovieService } from '@core/services/movie.service';

@Component({
  selector: 'app-watch-list',
  templateUrl: './watch-list.component.html',
  styleUrls: ['./watch-list.component.scss']
})
export class WatchListComponent {

  @ViewChild( MatGridList, {static: false} )
  private gridList: MatGridList;


  public searchTitle = 'Christmas';
  public get title() { return this.searchTitle }
  public set title( newTitle: string ) {
    this.searchTitle = newTitle;
  }

  constructor(
    private movieService: MovieService,
  ) {
    setTimeout( () => { this.onResize( null ); }, 100 );
    this.onSubmit() ;
  }


  public onSubmit() {
    this.movieService.queryMovieByTitle( this.searchTitle )
      .subscribe( () => {
        this.onMoviesChanged( [] );
      } );
  }


  public foundMovies: Movie[] = [];
  public selectedMovies: Movie[] = [];
  public watchedMovies: Movie[] = [];

  public onMoviesChanged( movies: Movie[] ) {
    this.movieService.movies = this.movieService.movies.filter( movie => ( movie.toWatch || !movie.toDitch ) );
    this.foundMovies = this.movieService.movies.filter( movie => ( !movie.toWatch && !movie.watched ) );
    this.selectedMovies = this.movieService.movies.filter( movie => (  movie.toWatch && !movie.watched ) );
    this.watchedMovies = this.movieService.movies.filter( movie => (  movie.toWatch &&  movie.watched ) );
  }


  @HostListener('window:resize', ['$event'] )
  private onResize( event: Event) {
    if ( this.gridList && this.gridList ) this.gridList.rowHeight = ( window.innerHeight / 2 - 100 ) + 'px';
  }

}
