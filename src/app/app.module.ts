import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppCoreModule } from './core/app-core.module';
import { AppPagesModule } from './pages/app-pages.module';
import { AppRoutingModule } from './pages/app-routing.module';

import { AuthKeyInterceptor }  from './core/services/auth-key.interceptor';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppCoreModule,
    AppPagesModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthKeyInterceptor, multi: true },
  ],
  bootstrap: [ AppComponent ],
  entryComponents: [  ],
})
export class AppModule { }
