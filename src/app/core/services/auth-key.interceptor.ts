import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthKeyInterceptor implements HttpInterceptor {

  constructor() {}

  // add the API key provided to the url
  intercept( request: HttpRequest< unknown >, next: HttpHandler): Observable< HttpEvent< unknown > > {
    return next.handle( request.clone( {
      url: request.url + '&apikey=186be766'
    } ) );
  }
}
