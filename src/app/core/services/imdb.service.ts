import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { Movie } from '@core/objects/movie';

@Injectable({
  providedIn: 'root'
})
export class ImdbService {

/** ref: http://www.omdbapi.com/ **/
  private urlRoot = 'http://www.omdbapi.com/';
  private imgRoot = 'http://img.omdbapi.com/';

  constructor(
    private http: HttpClient,
  ) { }

  public queryMovieByTitle( title: string ): Observable< Movie[] > {
    if ( !title ) return of( null )
    return this.http.get( this.urlRoot + '?s=' + title )
      .pipe( map ( ress => ress['Search'].map( res => new Movie( res ) ) ) );
      // TODO error catcher
  }

  public getMovieByTitle( title: string ): Observable< Movie > {
    if ( !title ) return of( null )
    return this.http.get( this.urlRoot + '?t=' + title )
      .pipe( map ( res => new Movie( res ) ) );
      // TODO error catcher
  }

  public getMovieByID( id: string ): Observable< Movie > {
    if ( !id ) return of( null )
    return this.http.get( this.urlRoot + '?i=' + id )
      .pipe( map ( res => new Movie( res ) ) );
      // TODO error catcher
  }

  public getImageByID( id: string ): Observable< Movie > {
    if ( !id ) return of( null )
    return this.http.get( this.imgRoot + '?i=' + id )
      .pipe( map ( res => new Movie( res ) ) );
      // TODO error catcher
  }

  private title2arg( title: string ): string {
    return encodeURI( title );
  }

}
