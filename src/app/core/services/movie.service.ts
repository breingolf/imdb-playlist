import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { Movie } from '@core/objects/movie';
import { ImdbService } from '@core/services/imdb.service';


@Injectable({
  providedIn: 'root'
})
export class MovieService {

  public movies: Movie[] = [] ;

  constructor(
    private imdbService: ImdbService,
  ) {
  }

  public getMovieByTitle( title: string ): Observable< Movie > {
    return this.imdbService.getMovieByTitle( title )
      .pipe( map ( movie => {
        if ( movie ) {
          let existing = this.movies.filter( xmovie => xmovie.id===movie.id );
          if ( !existing.length ) this.movies.push( movie );
        }
        return movie;
      } ) );
  }

  public queryMovieByTitle( title: string ): Observable< Movie[] > {
    return this.imdbService.queryMovieByTitle( title )
      .pipe( map ( movies => {
        if ( movies && movies.length ) {
          movies.forEach( movie => {
            let existing = this.movies.filter( xmovie => xmovie.id===movie.id );
            if ( !existing.length ) this.movies.push( movie );
          } );
        }
        return movies;
      } ) );
  }


}
