export class Movie {

  public id = '';
  public toWatch = false;
  public watched = false;
  public toDitch = false;
  public myRatings: number[] = [];

  public title: string;
  public year: string;
  public genre: string;
  public released: string;
  public director: string;
  public actors: string[];
  public runtime: string;

  constructor( res: any ) {
    if ( res && res.imdbID ) {
      this.title = res.Title ;
      this.year = res.Year ;
      this.id = res.imdbID;
      if ( res.genre ) this.genre = res.Genre;
      if ( res.released ) this.released = res.Released; // TODO convert to date using moment && some human time interpretation library
      if ( res.director ) this.director = res.Director;
      if ( res.actors ) this.actors = ( < string > res.Actors ).split(',').map( actor => actor.trim() );
      if ( res.runtime ) this.runtime = res.Runtime; // TODO convert to duration using moment && some human time interpretation library
    }
  }

  /* IMDB response to get like:
    Title: "2001: A Space Odyssey"
    Year: "1968"
    Rated: "G"
    Released: "12 May 1968"
    Runtime: "149 min"
    Genre: "Adventure, Sci-Fi"
    Director: "Stanley Kubrick"
    Writer: "Stanley Kubrick (screenplay), Arthur C. Clarke (screenplay)"
    Actors: "Keir Dullea, Gary Lockwood, William Sylvester, Daniel Richter"
    Plot: "After discovering a mysterious artifact buried beneath the Lunar surface, mankind sets off on a quest to find its origins with help from intelligent supercomputer H.A.L. 9000."
    Language: "English, Russian"
    Country: "USA, UK"
    Awards: "Won 1 Oscar. Another 15 wins & 10 nominations."
    Poster: "https://m.media-amazon.com/images/M/MV5BMmNlYzRiNDctZWNhMi00MzI4LThkZTctMTUzMmZkMmFmNThmXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg"
    Ratings: (3) [{…}, {…}, {…}]
    Metascore: "84"
    imdbRating: "8.3"
    imdbVotes: "566,801"
    imdbID: "tt0062622"
    Type: "movie"
    DVD: "25 Aug 1998"
    BoxOffice: "$135,620"
    Production: "Warner Bros. Pictures"
    Website: "N/A"
    Response: "True"
  */
  /* IMDB response to search like:
    Title: "2001: A Space Odyssey"
    Year: "1968"
    Poster: "https://m.media-amazon.com/images/M/MV5BMmNlYzRiNDctZWNhMi00MzI4LThkZTctMTUzMmZkMmFmNThmXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg"
    imdbID: "tt0062622"
    Type: "movie"
  */
   /* IMDB response like:
    Response: "False",
    Error: "Movie not found!"
  */

}
