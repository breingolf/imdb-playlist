import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Movie } from '@core/objects/movie';


@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {

  @Input()
  public movies: Movie[] = [];

  @Output()
  public moviesChanged = new EventEmitter< Movie[] >();

  constructor() { }

  ngOnInit(): void {
  }

  public onMovieChange( movie: Movie ) {
    this.moviesChanged.emit( this.movies );
  }
}
