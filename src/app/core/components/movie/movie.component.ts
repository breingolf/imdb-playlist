import { Component, OnInit, Input, Output, HostListener, EventEmitter } from '@angular/core';

import { Movie } from '@core/objects/movie';


@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent {

  @Input()
  public movie: Movie;
  @Output()
  public movieChange = new EventEmitter< Movie >();

  constructor(
  ) { }



  @HostListener('click', ['$event'])
  onEvent( event: MouseEvent ) {
    if ( event.ctrlKey ) {
      if ( !this.movie.toWatch ) this.movie.toDitch = !this.movie.toDitch;
    }
    else if ( event.shiftKey ) {
      if ( this.movie.toWatch ) this.movie.watched = !this.movie.watched;
    }
    else {
      if ( !this.movie.watched ) this.movie.toWatch = !this.movie.toWatch;
    }
    this.movieChange.emit( this.movie );
  }



  public ratingEditable = false;
  public newRating = '';

  public editRating() {
    this.ratingEditable = true;
  }

  public onSubmit( event: MouseEvent ) {
    event.stopPropagation();
    let rating = -1;
    try {
      rating = +this.newRating;
    }
    catch(e){}
    if ( 1<= rating && rating <=5 ) this.movie.myRatings.push( rating );
    this.ratingEditable = false;
    console.log( this.ratingEditable );
  }

}
