/* see also: https://www.hongkiat.com/blog/guide-to-using-sublimelinter4/, also -gjslint, -annotations */
/** https://stylelint.io/user-guide/configuration **/
  /* stylelintrc.json can use "extends": "stylelint-config-standard", */
module.exports = {
  "env": {
    "browser": true,
    "node": true
  },
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "project": "tsconfig.json",
    "sourceType": "module",
    "extraFileExtensions": [".vue", ".html"]
  },
  "plugins": [
    "@typescript-eslint",
    "@typescript-eslint/tslint",
    "html"
  ],
  "rules": {
    "explicit-member-accessibility": [ "off", { "accessibility": "explicit" } ],
    "indent": [ "warn", 2 ],
    "member-ordering": [ "off", {
      "default": [
        "abstract-field",
        "abstract-method",
        "instance-field",
        /* "instance-accessor", */
        "constructor",
        "instance-method",
        "static-field",
        "static-method"
      ]
    } ],
    "no-empty-function": "off",
    "no-use-before-define": "error",
    "quotes": [ "warn", "single" ],
    "semi": [ "error", "always" ],
    "arrow-body-style": "error",
    "camelcase": "off",
    "constructor-super": "error",
    "curly": ["error", "multi-line"],
    "dot-notation": "off",
    "eol-last": "error",
    /*"eqeqeq": [
      "error",
      "smart"
    ],*/
    "grouped-accessor-pairs": ["error", "getBeforeSet"],
    "guard-for-in": "error",
    "id-blacklist": "off",
    "id-match": "off",
    "max-len": ["warn", { "code": 200 } ],
    "no-bitwise": "off",
    "no-caller": "error",
    "no-console": [ "error", {
        "allow": [
          "Console", "log", "warn", "error",
          "assert", "clear", "context", "count", "countReset", "dir", "dirxml", "group", "groupCollapsed", "groupEnd", "profile", "profileEnd", "table", "timeLog", "timeStamp"
        ]
      } ],
    "no-debugger": "error",
    /* "no-deprecated": "warn", */
    "no-empty": "off",
    "no-eval": "error",
    "no-fallthrough": "error",
    "no-new-wrappers": "error",
    "no-shadow": [ "error", { "hoist": "all" } ],
    "no-throw-literal": "error",
    "no-trailing-spaces": "warn",
    "no-undef-init": "error",
    "no-underscore-dangle": "off",
    "no-unused-expressions": "error",
    "no-unused-labels": "error",
    "no-var": "error",
    "prefer-const": "error",
    "radix": ["error", "as-needed"],
    "spaced-comment": "warn",
    "array-bracket-spacing": ["warn", "always"],
    "object-curly-spacing": ["warn", "always"],
    "space-in-parens": ["warn", "always", { "exceptions": [ "empty" ] } ],
    "space-infix-ops": ["warn", { "int32Hint": true }],
    "space-unary-ops": ["warn", { "words": true, "nonwords": false, "overrides": { /* "new": false, "++": false */ } }],
    /* "space-in-brackets": ["warn", "always" ], */

    "@typescript-eslint/prefer-function-type": "error",
    "@typescript-eslint/class-name-casing": "error",
    "@typescript-eslint/consistent-type-definitions": "error",
    "@typescript-eslint/member-delimiter-style": [ "error", {
        "multiline": { "delimiter": "semi", "requireLast": true },
        "singleline": { "delimiter": "semi", "requireLast": false }
      } ],
    "@typescript-eslint/no-empty-interface": "error",
    "@typescript-eslint/no-inferrable-types": "error",
    "@typescript-eslint/no-misused-new": "error",
    "@typescript-eslint/no-non-null-assertion": "error",
    "@typescript-eslint/type-annotation-spacing": "error",
    "@typescript-eslint/unified-signatures": "error",

    "@typescript-eslint/tslint/config": [ "error", {
      "rules": {
        "import-blacklist": [ true, "rxjs/Rx" ],
        "import-spacing": false,
        "one-line": { "severity": "warning", "options": [
          "check-open-brace",
          "check-whitespace"
        ] },
        "whitespace": { "severity": "warning", "options": [
          "check-branch",
          "check-decl",
          "check-module",
          /*"check-operator",*/
          "check-separator",
          "check-type",
          "check-typecast",
          "check-type-operator",
          "check-preblock"
        ] }
        /*
        "component-class-suffix": true,
        "component-selector": [
          true,
          "element",
          "app",
          "kebab-case"
        ],
        "directive-class-suffix": true,
        "directive-selector": [
          true,
          "attribute",
          "app",
          "camelCase"
        ],
        "no-conflicting-lifecycle": true,
        "no-host-metadata-property": true,
        "no-input-rename": true,
        "no-inputs-metadata-property": true,
        "no-output-native": true,
        "no-output-on-prefix": true,
        "no-output-rename": true,
        "no-outputs-metadata-property": true,
        "nx-enforce-module-boundaries": [
          true,
          {
            "allow": [],
            "depConstraints": [
              {
                "sourceTag": "*",
                "onlyDependOnLibsWithTags": [
                  "*"
                ]
              }
            ]
          }
        ],
        "template-banana-in-box": true,
        "template-no-negated-async": true,
        "use-lifecycle-interface": true,
        "use-pipe-transform-interface": true,
      */
      }
    }
  ] },
  "settings": { // for html plugin
    "html/indent": "+2",  // indentation is the <script> indentation plus two spaces.
  }
};
