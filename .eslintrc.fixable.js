/* see also: https://www.hongkiat.com/blog/guide-to-using-sublimelinter4/, also -gjslint, -annotations */
/** https://stylelint.io/user-guide/configuration **/
  /* stylelintrc.json can use "extends": "stylelint-config-standard", */
module.exports = {
  "env": {
    "browser": true,
    "node": true
  },
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "project": "tsconfig.json",
    "sourceType": "module",
    "extraFileExtensions": [".vue", ".html"]
  },
  "plugins": [
    "@typescript-eslint",
    "@typescript-eslint/tslint"
  ],
  "rules": {
    "no-bitwise": "off",
    "no-empty": "off",
    "prefer-const": "off",
    "@typescript-eslint/class-name-casing": "error",
    "@typescript-eslint/indent": [ "warn", 2 ],
    "@typescript-eslint/quotes": [ "warn", "single" ],
    "@typescript-eslint/semi": [ "error", "always" ],
    "@typescript-eslint/type-annotation-spacing": "error",
    "eol-last": "error",
    "no-trailing-spaces": "error",
    "spaced-comment": "error",
    "array-bracket-spacing": ["warn", "always"],
    "object-curly-spacing": ["warn", "always"],
    "space-in-parens": ["warn", "always", { "exceptions": [ "empty" ] } ],
    "space-infix-ops": ["warn", { "int32Hint": true }],
    "space-unary-ops": ["warn", { "words": true, "nonwords": false, "overrides": { /* "new": false, "++": false */ } }],
    /* "space-in-brackets": ["warn", "always" ], */
  }
};
